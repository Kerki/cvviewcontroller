//
//  skillsViewController.swift
//  CVViewController
//
//  Created by Erik Granlund on 2019-11-06.
//  Copyright © 2019 Erik Granlund. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {

    
    @IBOutlet weak var pressAnimation: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pressAnimation.setTitle("Pulsate", for: .normal)
    }
    
    @IBAction func animationButton(_ sender: UIButton) {
        sender.pulsate()

    }
    
    
    
    @IBAction func dismissButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
   
}
