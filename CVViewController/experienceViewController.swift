//
//  experienceViewController.swift
//  CVViewController
//
//  Created by Erik Granlund on 2019-11-15.
//  Copyright © 2019 Erik Granlund. All rights reserved.
//

import UIKit

class ExperienceViewController: UITableViewController {

    
    @IBOutlet weak var workTable: UITableView!
    
    var experienceArray: [[Experience]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var work:[Experience] = []
        var education:[Experience] = []
        
        workTable.delegate = self
        workTable.dataSource = self
        for i in 0..<2 {
            let experienceWork = Experience(imageName: "Ica" ,name: "work \(i+1)", dates: "201\((i+3)%10) - 201\((i+4)%10)")
            work.append(experienceWork)
        }
        for i in 0..<1 {
            let experienceEducation = Experience(imageName: "jönköping" ,name: "education \(i+1)", dates: "2018 - now")
            education.append(experienceEducation)
        }
        experienceArray.append(work)
        experienceArray.append(education)
    
        workTable.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExperienceDetailViewController {
            if let indexPath = sender as? IndexPath {
                let experienceDetail = experienceArray[indexPath.section][indexPath.row]
                destination.experience = experienceDetail
            }
        }
    }
    
}

extension ExperienceViewController{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experienceArray[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceCell") as? ExperienceTableViewCell{
            var experienceItem = experienceArray[indexPath.section][indexPath.row]
            cell.workImage.image = UIImage(named: experienceItem.imageName)
            cell.workName.text = experienceItem.name
            cell.workSpan.text = experienceItem.dates
            return cell
        }
        return UITableViewCell()
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionName = ["Work", "Education"]
        return sectionName[section]
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailSegue", sender: indexPath)
    }

}
