//
//  animation.swift
//  CVViewController
//
//  Created by Erik Granlund on 2019-12-08.
//  Copyright © 2019 Erik Granlund. All rights reserved.
//

import Foundation

import UIKit

extension UIButton {
    
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 3.4
        pulse.fromValue = 1
        pulse.toValue = 1.4
        pulse.autoreverses = true
        pulse.repeatCount = 5
        pulse.initialVelocity = 0.7
        pulse.damping = 1.5
        
        layer.add(pulse, forKey: nil)
    }
}
