//
//  ExperienceDetailViewController.swift
//  CVViewController
//
//  Created by Erik Granlund on 2019-12-08.
//  Copyright © 2019 Erik Granlund. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var detailTitle: UILabel!
    @IBOutlet weak var detailSpan: UILabel!
    @IBOutlet weak var detailDetails: UILabel!
    
    var experience: Experience = Experience(imageName: "", name: "", dates: "", details: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailImage.image = UIImage(named: experience.imageName)
        detailTitle.text = experience.name
        detailSpan.text = experience.dates
        detailDetails.text = experience.details
        
        self.navigationItem.title = experience.name
    }
}
