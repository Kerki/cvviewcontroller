//
//  experienceTableViewCell.swift
//  CVViewController
//
//  Created by Erik Granlund on 2019-11-08.
//  Copyright © 2019 Erik Granlund. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
   
    
    @IBOutlet weak var workImage: UIImageView!
    
    
    @IBOutlet weak var workName: UILabel!
    @IBOutlet weak var workSpan: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}

