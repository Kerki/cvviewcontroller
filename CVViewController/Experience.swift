//
//  work.swift
//  CVViewController
//
//  Created by Erik Granlund on 2019-11-08.
//  Copyright © 2019 Erik Granlund. All rights reserved.
//

import Foundation

class Experience  {
    var imageName: String
    var name: String
    var dates: String
    var details: String
    
    init(imageName: String = "", name: String = "", dates: String = "", details: String = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." ) {
        self.imageName = imageName
        self.name = name
        self.dates = dates
        self.details = details
    }
}
