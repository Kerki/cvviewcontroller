//
//  education.swift
//  CVViewController
//
//  Created by Erik Granlund on 2019-11-20.
//  Copyright © 2019 Erik Granlund. All rights reserved.
//

import Foundation

class education  {
    let imageName: String
    let name: String
    var dates: String
    
    init(imageName: String = "default", name: String = "education", dates: String = "dates") {
        self.imageName = imageName
        self.name = name
        self.dates = dates
    }
}
